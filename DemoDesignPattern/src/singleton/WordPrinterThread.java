/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author hoang
 */
public class WordPrinterThread extends Thread {

    PrinterDriver driver;
    PrinterData printer_data;
    public static int DELAY_TIME = 200;

    void initPrinterDriver(PrinterData data){
        printer_data = data;
        driver = PrinterDriver.getInstance(printer_data);
    }
    
    @Override
    public synchronized void start() {
        super.start();
        initPrinterDriver(printer_data);
    }    
    
    @Override
    public void run() {
        try {
            while (true) {
                System.out.println(
                        String.format("Thread %s: %s", getName(), driver.runningMessage())
                );                
                sleep(DELAY_TIME);
            }
        }catch(InterruptedException interruptedException){
            System.out.println("Interrupted.");
        }
    }
       

}
