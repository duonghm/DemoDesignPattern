/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author hoang
 */
public class PrinterDriver {

    private PrinterData data;
    
    static private PrinterDriver uniqueInstance;
    
    private PrinterDriver() {        
    }
    
    public void setPrinterData(PrinterData data){
        this.data = data;
    }
    
    public String runningMessage(){
        return String.format(
                "The printer model %s version %s is running ...... - %d", 
                data.getModel(), data.getVersion(), this.hashCode()
        );        
    }
    
    public static PrinterDriver getInstance(PrinterData printerData){
        if(uniqueInstance==null){
            uniqueInstance = new PrinterDriver();
            uniqueInstance.setPrinterData(printerData);
        }
        return uniqueInstance;
    }
    
    
}
