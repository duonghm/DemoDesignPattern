/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author hoang
 */
public class SingletonTest {
    
    static final int NUMBER_OF_THREADS = 5;
    
    public static void main(String[] args) {        
        WordPrinterThread[] threads = new WordPrinterThread[NUMBER_OF_THREADS];
        for(int i=0; i<NUMBER_OF_THREADS; i++){
            threads[i] = new WordPrinterThread();
            threads[i].initPrinterDriver(PrinterData.initDataFromModelAndVersion("HP"+i, "6630"+i));
            threads[i].setName(String.valueOf(i));
        }
        for(int i=0; i<NUMBER_OF_THREADS; i++){
            threads[i].start();
        }
    }
    
}
