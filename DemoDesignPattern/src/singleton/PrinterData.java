/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author hoang
 */
public final class PrinterData {
    private String model;
    private String version;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    private PrinterData() {
    }
    
    static PrinterData initDataFromModelAndVersion(String model, String version){
        PrinterData data = new PrinterData();
        data.setModel(model);
        data.setVersion(version);
        return data;
    }
}
